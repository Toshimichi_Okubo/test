let [walkX, walkY] = [0, 0];
let fightOrRun;
let runDec;
let [encountCnt, killCnt, walkCnt, perEnc, heroLvl] = [0, 0, 0, 0.3, 1];
let xRes = document.getElementById("xResult");
let yRes = document.getElementById("yResult");
let hLog = document.getElementById("heroLog");
let hWrap = document.getElementById("heroWrap");
console.log(perEnc);
const [winMsg, loseMsg, runMsg, runFMsg, encMsg, fieldMsg, lvlMsg] = ["敵を倒した!", "ヒーローは死んでしまった", "逃げられた", "逃げられない", "敵が現れた", "歩く方向を指定してください", "<br>レベルが上がりました!"];

class btnShow {
    battleBtnShow() {
        enemyPict.style.display = "inline";
        fightBtn.style.display = "inline";
        runBtn.style.display = "inline";
    }
    battleBtnNotShow() {
        enemyPict.style.display = "none";
        fightBtn.style.display = "none";
        runBtn.style.display = "none";
    }
    menuBtnShow() {
        goStraight.style.display = "inline";
        goBack.style.display = "inline";
        goLeft.style.display = "inline";
        goRight.style.display = "inline";
    }
    menuBtnNotShow() {
        goStraight.style.display = "none";
        goBack.style.display = "none";
        goLeft.style.display = "none";
        goRight.style.display = "none";
    }

    logMsgList(x) {
        switch (x) {
            case "enc":
                hLog.innerHTML = encMsg;
                break;
            case "field":
                hLog.innerHTML = fieldMsg;
                break;
            case "win":
                hLog.innerHTML = winMsg;
                break;
            case "lose":
                hLog.innerHTML = loseMsg;
                break;
            case "run":
                hLog.innerHTML = runMsg;
                break;
            case "runng":
                hLog.innerHTML = runFMsg;
                break;
            case lvl:
                hLog.innerHTML = hLog.innerHTML + lvlMsg;
                break;
        }
    }
    bgChange(y) {
        switch (y) {
            case "numa":
                console.log("沼");
                hWrap.style.backgroundImage = "url(../images/numa.jpg)";
                break;
            case "forest":
                console.log("森");
                hWrap.style.backgroundImage = "url(../images/forest.jpg)";
                break;
            case "heigen":
                console.log("平原");
                hWrap.style.backgroundImage = "url(../images/heigen.jpg)";
                break;
        }
    }
    pictChange(z) {
        switch (z) {
            case 0:
                enemyPict.src = "images/dead.png";
                break;
            case 1:
                heroPict.src = "images/dead.png";
                break;
            case 2:
                enemyPict.src = "images/enemy.png";
                break;
            case 3:
                heroPict.src = "images/hero.png";
                break;
        }
    }
}


class fieldMenu extends btnShow {
    constructor(direct) {
        super();
        this.direct = direct;
    }

    encount() {
        if (Math.random() < perEnc) {
            encountCnt++;
            this.encLogShow();
            this.battleReady();
        }
    }

    xyWalkCalc() {
        switch (this.direct) {
            case "goStraight":
                walkY++;
                break;
            case "goBack":
                walkY--;
                break;
            case "goRight":
                walkX++;
                break;
            case "goLeft":
                walkX--;
                break;
        }
        xRes.innerHTML = walkX;
        yRes.innerHTML = walkY;
    }

    battleReady() {
        super.logMsgList("enc");
        super.battleBtnShow();
        super.menuBtnNotShow();
        fightOrRun = new battleMenu(heroLvl);
        runDec = new battleMenu(heroLvl);
    }

    heroPrace() {
        if (Math.abs(walkX) > 20 || Math.abs(walkY) > 20) {
            perEnc = 0.7;
            super.bgChange("numa");
        } else if (Math.abs(walkX) > 10 || Math.abs(walkY) > 10) {
            perEnc = 0.5;
            super.bgChange("forest");
        } else {
            perEnc = 0.3;
            super.bgChange("heigen");
        }
    }

    encLogShow() {
        document.getElementById("enemyLog").innerHTML = document.getElementById("enemyLog").innerHTML + `${walkCnt}歩目
`;
    }

}


class battleMenu extends btnShow {
    constructor(nowLvl) {
        super();
        this.lvl = nowLvl;
    }

    fight() {
        console.log(heroLvl);
        if (Math.random() < 0.6 + (0.09 * heroLvl)) {
            this.win();
            return false;
        } else {
            this.lose();
            return true;
        }
    }

    win() {
        killCnt++;
        super.logMsgList("win");
        super.battleBtnNotShow();
        enemyPict.style.display = "inline";
        super.pictChange(0);
        if (killCnt % 10 === 0) {
            heroLvl++;
            super.logMsgList("lvl");
            document.getElementById("lvl").innerHTML = heroLvl;
        }
        setTimeout(this.battleDelayShow, 1500);
    }

    lose() {
        super.logMsgList("lose");
        super.battleBtnNotShow();
        enemyPict.style.display = "inline";
        super.pictChange(1);
        super.menuBtnNotShow();
        this.resultShow();
    }

    run() {
        console.log(this);
        super.battleBtnNotShow();
        if (Math.random() < 0.5) {
            super.logMsgList("run");
            super.menuBtnShow();
            setTimeout(() => {
                super.logMsgList("field");
            }, 1000);
        } else {
            super.logMsgList("runng");
            setTimeout(this.runNg, 1000, this);
        }
    }

    runNg(that) {
        console.log(that);
        if (Math.random() < 0.6 + (0.09 * heroLvl)) {
            console.log("生存");
            super.battleBtnShow();
        } else {
            console.log(that + "死亡");
            that.lose();
        }
    }

    battleDelayShow() {
        console.log("戦闘終了");
        super.logMsgList("field");
        super.menuBtnShow();
        enemyPict.style.display = "none";
        super.pictChange(2);
    }

    resultShow() {
        document.getElementById("enemyResult").innerHTML = killCnt;
        document.getElementById("enemyPerWalk").innerHTML = Math.round(walkCnt / encountCnt);
    }
}


document.querySelector(".directSelect").addEventListener('click', function (aa) {
    if (aa.target.id) {
        let dirDec = new fieldMenu(aa.target.id);
        walkCnt++;
        dirDec.xyWalkCalc();
        dirDec.heroPrace();
        dirDec.encount();
    }
});

document.getElementById("fightBtn").addEventListener('click', function () {
    fightOrRun.fight();
});

document.getElementById("runBtn").addEventListener('click', function () {
    runDec.run();
});

document.getElementById("startBtn").addEventListener('click', function () {
    let reset = new btnShow();
    console.log(reset);
    reset.menuBtnShow();
    reset.battleBtnNotShow();
    [walkX, walkY] = [0, 0];
    [encountCnt, killCnt, walkCnt, perEnc, heroLvl] = [0, 0, 0, 0.3, 1];
    reset.pictChange(2);
    reset.pictChange(3);
    reset.bgChange("heigen");
    reset.logMsgList("field");
    document.getElementById("enemyLog").innerHTML = "";
    document.getElementById("lvl").innerHTML = 1;

});

window.location.hash = "no-back";
window.location.hash = "no-back-button";
window.onhashchange = function () {
    window.location.hash = "no-back";
}
document.addEventListener("keydown", function (e) {

    if ((e.which || e.keyCode) == 116) {
        e.preventDefault();
        console.log("F5禁止!");
    }

});


/*
平地3割　森5割　沼７割
*/
